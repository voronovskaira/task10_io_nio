package pushbackstream;

import java.io.ByteArrayInputStream;
import java.io.IOException;

public class PushBackStreamTest {
    public static void main(String[] args) {

        String data = "This an example of PushbackInputStream";

        ByteArrayInputStream byteArrayInputStream = null;
        CustomInputStream pushbackInputStream = null;
        try {
            byteArrayInputStream = new ByteArrayInputStream(data.getBytes());
            pushbackInputStream = new CustomInputStream(byteArrayInputStream, 20);

            //Read first character from stream
            int i = pushbackInputStream.read();
            int a = pushbackInputStream.read();
            System.out.print((char) i);
            System.out.println((char) a);
            pushbackInputStream.unread(a);

            //Push back first character to stream
            //pushbackInputStream.unread(i);
            //Now Read full bytes
            byte b[] = new byte[data.getBytes().length];
            pushbackInputStream.read(b);
            System.out.println(new String(b));

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (pushbackInputStream != null) {
                    pushbackInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
