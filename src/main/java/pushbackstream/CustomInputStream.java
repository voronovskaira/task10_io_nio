package pushbackstream;

import java.io.IOException;
import java.io.InputStream;

public class CustomInputStream extends InputStream {
    InputStream in;
    byte[] buf;
    int index = 0;

    public CustomInputStream(InputStream in, int bufSize) {
        this.in = in;
        this.buf = new byte[bufSize];
    }

    public void unread(int b) throws IOException {
        if (index == buf.length) {
            throw new IOException("Push back buffer is full");
        }
        buf[index++] = (byte) b;
    }

    @Override
    public int read() throws IOException {
        if (index == 0) {
            return in.read();
        } else {
            return buf[--index];
        }
    }
}
