package nio;

public class UnknownCommandException extends Exception{
    @Override
    public String getMessage() {
        return "Command don't exist";
    }
}
