package nio;

import nio.view.ConsoleView;
import nio.view.View;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Application {
    static View view = new ConsoleView();
    static CommandHandler commandHandler = new CommandHandler(new ConsoleView());

    public static void main(String[] args) {

        while (true) {
            try {
                Scanner scanner = new Scanner(System.in);
                String value = scanner.nextLine();
                commandHandler.executeCommand(value);
            } catch (UnknownCommandException e) {
                view.print("Error, try another command");
            } catch (FileNotFoundException e) {
                view.print("File not found");
            } catch (IOException e) {
                view.print("IO exception");
            }
        }


    }
}
