package nio.view;

public interface View {
    void print(String message);
}
