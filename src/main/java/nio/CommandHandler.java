package nio;

import nio.view.View;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandHandler {
    private CommandLine commandLine;


    public CommandHandler(View view) {
        this.commandLine = new CommandLine(view);

    }

    public void executeCommand(String command) throws UnknownCommandException, IOException {
        List<String> commandList = new ArrayList<>(Arrays.asList(command.split(" ")));
        if (command.equals("dir")) {
            commandLine.showFoldersList();
        } else if (commandList.size() == 2 && commandList.get(0).equals("cd")) {
            commandLine.changeFolder(commandList.get(1));
        } else if(commandList.size() == 2 && commandList.get(0).equals("cat")){
            commandLine.readJavaComments(commandList.get(1));
        }
        else
            throw new UnknownCommandException();
    }


}
