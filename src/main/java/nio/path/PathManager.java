package nio.path;

public interface PathManager {
    String getCurrentPath();
    void changeCurrentPath(String path);
    void changeCurrentPath();
}
