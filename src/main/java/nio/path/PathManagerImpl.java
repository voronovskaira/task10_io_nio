package nio.path;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;

public class PathManagerImpl implements PathManager {

    private Deque<String> currentPath;

    public PathManagerImpl() {
        this.currentPath = new LinkedList<>(Arrays.asList(System.getProperty("user.dir").split("/")));
    }

    @Override
    public String getCurrentPath() {
        StringBuilder path = new StringBuilder();
        currentPath.forEach(p -> path.append("/" + p));
        return path.toString();
    }

    @Override
    public void changeCurrentPath(String path) {
        currentPath.addLast(path);
    }

    @Override
    public void changeCurrentPath() {
        currentPath.removeLast();
    }
}
