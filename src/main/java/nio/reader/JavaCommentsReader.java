package nio.reader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.IntStream;

public class JavaCommentsReader {

    public String readJavaComments(String filePath) {
        String comment = "";
        Path path = Paths.get(filePath);
        try {
            List<String> contents = Files.readAllLines(path);
            comment = parseText(contents);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return comment;
    }

    private String parseText(List<String> text) {
        StringBuilder comments = new StringBuilder();
        IntStream.range(0, text.size()).forEach(index -> {
            if (text.get(index).trim().startsWith("//")) {
                comments.append(text.get(index)).append("\n");
            } else if (text.get(index).trim().startsWith("/**") || text.get(index).trim().startsWith("/*")) {
                do {
                    comments.append(text.get(index)).append("\n");
                } while (!text.get(index++).endsWith("*/"));
            }
        });
        return comments.toString();
    }

}
