package nio;

import nio.path.PathManager;
import nio.path.PathManagerImpl;
import nio.reader.JavaCommentsReader;
import nio.view.View;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class CommandLine {

    private PathManager pathManager;
    private View view;
    private JavaCommentsReader commentsReader;

    CommandLine(View view) {
        this.pathManager = new PathManagerImpl();
        this.view = view;
        this.commentsReader = new JavaCommentsReader();
    }

    public void showFoldersList() throws IOException {
        Stream<Path> path = Files.list(Paths.get(pathManager.getCurrentPath()));
        path.forEach(p -> view.print(p.getFileName().toString()));
    }

    public void changeFolder(String path) throws UnknownCommandException {
        if (path.equals("..")) {
            pathManager.changeCurrentPath();
            view.print("You in " + pathManager.getCurrentPath());
        } else if (Files.isDirectory(Paths.get(pathManager.getCurrentPath() + "/" + path))) {
            pathManager.changeCurrentPath(path);
            view.print("You in " + pathManager.getCurrentPath());
        } else
            throw new UnknownCommandException();
    }

    public void readJavaComments(String filePath) throws FileNotFoundException {
        if (Files.isRegularFile(Paths.get(pathManager.getCurrentPath() + "/" + filePath)) && filePath.endsWith(".java")) {
            String comments = commentsReader.readJavaComments(pathManager.getCurrentPath() + "/" + filePath);
            view.print(comments);
        } else
            throw new FileNotFoundException();

    }


}
